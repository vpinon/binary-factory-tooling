#!/usr/bin/env bash
set -e

# Determine the resources we are going to work on
APPIMAGE_PATH="${1}"
GPG_KEY="FE17AD4610811CB7"

if [ ! -e $APPIMAGE_PATH ]; then
  echo "The appimage specified does not exist"
  exit 1
fi

# Create a workspace for ourselves
tempdir="$(mktemp sign_appimage.XXXXXX -d -p /tmp)"

# Make sure it is secure
chmod 700 $tempdir

# Determine the name of our appimage...
destination=$(basename $APPIMAGE_PATH)

# Derive the names of the gpg signature, sha256 sum digest and the gpg public key we will be providing
ascfile="${tempdir}/${destination}.digest.asc"
digestfile="${tempdir}/${destination}.digest"
sigkeyfile="${tempdir}/sig_pubkey"

# get offsets and lengths of .sha256_sig  and .sig_key sections of the AppImage
SIG_OFFSET=$(objdump -h "${APPIMAGE_PATH}" | grep .sha256_sig | awk '{print $6}')
SIG_LENGTH=$(objdump -h "${APPIMAGE_PATH}" | grep .sha256_sig | awk '{print $3}')

KEY_OFFSET=$(objdump -h "${APPIMAGE_PATH}" | grep .sig_key | awk '{print $6}')
KEY_LENGTH=$(objdump -h "${APPIMAGE_PATH}" | grep .sig_key | awk '{print $3}')

# Null the sections
dd if=/dev/zero bs=1 seek=$(($(echo 0x$SIG_OFFSET))) count=$(($(echo 0x$SIG_LENGTH))) of="${APPIMAGE_PATH}" conv=notrunc
dd if=/dev/zero bs=1 seek=$(($(echo 0x$KEY_OFFSET))) count=$(($(echo 0x$KEY_LENGTH))) of="${APPIMAGE_PATH}" conv=notrunc

# generate sha256sum
# BEWARE THE NEWLINE! if it is not stripped, AppImageUpdate validaton will fail
sha256sum $APPIMAGE_PATH | cut -d " " -f 1 | tr -d '\n' > $digestfile

# Sign the sha256sum
gpg --detach-sign --armor -u $GPG_KEY -o $ascfile $digestfile
gpg --export --armor $GPG_KEY > $sigkeyfile

# Embed the signature
dd if=${ascfile} bs=1 seek=$(($(echo 0x$SIG_OFFSET))) count=$(($(echo 0x$SIG_LENGTH))) of="${APPIMAGE_PATH}" conv=notrunc
# Embed the public part of the signing key
dd if=${sigkeyfile} bs=1 seek=$(($(echo 0x$KEY_OFFSET))) count=$(($(echo 0x$KEY_LENGTH))) of="${APPIMAGE_PATH}" conv=notrunc

# Cleanup
rm -rf $tempdir
